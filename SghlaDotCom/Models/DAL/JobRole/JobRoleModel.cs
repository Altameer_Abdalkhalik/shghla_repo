﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.JobRole
{
    /// <summary> 
    ///    هذا الجدول يشير الى الدور الوظيفي للموظف  
    /// </summary> 
    /// <remarks> 
            /// Longer comments can be associated with a type or member through 
            /// the remarks tag.
    /// </remarks> 
    public class JobRoleModel
    {
        public int ID { get; set; }

        public string jobRoleName { get; set; }
    }
}