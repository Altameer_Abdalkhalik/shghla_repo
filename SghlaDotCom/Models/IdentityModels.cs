﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using SghlaDotCom.Models.DAL.Tasks;
using System;
using SghlaDotCom.Models.DAL.Company;
using SghlaDotCom.Models.DAL.IssueReports;
using System.Collections.Generic;
using SghlaDotCom.Models.DAL.ApplicantRelated.Experience;
using SghlaDotCom.Models.DAL.ApplicantRelated.Skills;
using SghlaDotCom.Models.DAL.ApplicantRelated.Langauge;
using SghlaDotCom.Models.DAL.ApplicantRelated.Courses;
using SghlaDotCom.Models.DAL.ApplicantRelated.Projects;
using SghlaDotCom.Models.DAL.ApplicantRelated.Educations;
using SghlaDotCom.Models.DAL.ApplicantRelated.Certificates;
using SghlaDotCom.Models.DAL.ApplicantRelated.Awards;
using SghlaDotCom.Models.DAL.PositionLevel;

namespace SghlaDotCom.Models
{
    public enum gender_list
    {
        ذكر, أنثى
    }

    public enum married_Status
    {
        اعزب, متزوج
    }

    public enum userType
    {
        موظف, مستخدم
    }

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [ScaffoldColumn(false)]
        [Display(Name ="من قاعدة البيانات للموقع القديم")]
        public bool migratedUser { get; set; }

        [Display(Name = "*الاسم بالكامل")]
        [Required]
        public string fullName { get; set; }

        [Display(Name = "رقم الهاتف")]
        public string phoneNumber { get; set; }

        [Display(Name = "وصف")]
        public string Description { get; set; }

        [ScaffoldColumn(false)]
        public int? ApplicantInfoID { get; set; }
        public ApplicantInfo applicant { get; set; }

        [ScaffoldColumn(false)]
        public int? EmployerInfoID { get; set; }
        public EmployerInfo employer { get; set; }
    }


    public class ApplicantInfo
    {
        public int ID { get; set; }

        public ApplicationUser user { get; set; }

        [Display(Name = "*الجنس")]
        public gender_list gender { get; set; }

        [Display(Name = "تاريخ الميلاد")]
        public DateTime? birthDate { get; set; }

        public List<IssueReportsModel> reportsList { get; set; } // reporting specific job application

        public List<ExperienceModel> experienceList { get; set; }
        public List<CoursesModel> coursesList { get; set; } // one to many
        public List<ProjectsModel> projectList { get; set; } // one to many
        public List<AwardsModel> awardsList { get; set; } // 1 to many

        public List<SkillsWithApplicantsMRel> skillList { get; set; }
        public List<LanguageWithUsersMRel> langaugeList { get; set; }
        public List<EducationsWithApplicantsMRelModel> educationList { get; set; } // many to many
        public List<CertificatesWithUserMRelModel> certificatesList { get; set; } // many to many
    }

    public class EmployerInfo
    {
        public int ID { get; set; }

        public ApplicationUser user { get; set; }

        [Display(Name ="شركات")]
        public int CompanyModelID { get; set; }
        public CompanyModel company { get; set; } // if it doesn't exist create it

        public bool verified { get; set; }
    
        public int PositionLevelModelID { get; set; }
        public PositionLevelModel position { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        static ApplicationDbContext()
        {
            Database.SetInitializer(new MySqlInitializer());
        }

        public DbSet<DAL.ActivityLog.ActivityLogModel> ActivityLogTable { get; set; }
        public DbSet<DAL.SideBarItems.SideBarItemsModels> SideBarItemsTable { get; set; }
        public DbSet<DAL.AccessControl.AccessControlModel> AccessControlTable { get; set; }
        public DbSet<DAL.Notifications.NotificationsModel> NotificationsTable { get; set; }
        public DbSet<TasksModel> TasksTable { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}