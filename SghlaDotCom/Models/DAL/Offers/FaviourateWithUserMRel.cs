﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.Offers
{
    public class FaviourateWithUserMRel
    {
        public int ID { get; set; }

        public int? OffersModelID { get; set; }
        public int? ApplicationUserID { get; set; }

        public ApplicationUser user { get; set; }
        public OffersModel offer { get; set; }
    }
}