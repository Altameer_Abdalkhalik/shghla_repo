﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SghlaDotCom.Startup))]
namespace SghlaDotCom
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
