﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.PositionLevel
{
    public class PositionLevelModel
    {
        public int ID { get; set; }

        public string positionTitle { get; set; }

        public int rank { get; set; } // position within company are ranked according to this number

        public List<EmployerInfo> employeeList { get; set; }
    }
}