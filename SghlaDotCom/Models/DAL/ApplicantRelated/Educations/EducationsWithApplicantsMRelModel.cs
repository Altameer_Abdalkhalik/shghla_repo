﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Educations
{
    public class EducationsWithApplicantsMRelModel
    {
        public int ID { get; set; }

        public int EducationsModelID { get; set; }
        public int ApplicantInfoID { get; set; }

        public ApplicantInfo user { get; set; }
        public EducationsModel education { get; set; }
    }
}