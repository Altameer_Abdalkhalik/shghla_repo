﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SghlaDotCom.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress(ErrorMessageResourceName ="EmailAddress", ErrorMessageResourceType =typeof(Resources.Login))]
        [Display(Name = "Email", ResourceType = typeof(Resources.Login))]
        public string Email { get; set; }

        [Required]
        [StringLength(100, /*ErrorMessage = "The {0} must be at least {2} characters long.",*/
            ErrorMessageResourceType =typeof(Resources.Login), ErrorMessageResourceName = "MinimumLength", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Resources.Login))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword", ResourceType =typeof(Resources.Login))]
        [Compare("Password"/*, ErrorMessage = "The Confirm Password and Password Fields don't match"*/, ErrorMessageResourceType = typeof(Resources.Login), 
            ErrorMessageResourceName = "passwordAndConfirmPasswordDonotMatch")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "FullName", ResourceType = typeof(Resources.Login))]
        [MaxLength(length: 40/*, ErrorMessage = "لايجب ان يتجاوز {} {0} حرف"*/,ErrorMessageResourceType =typeof(Resources.Login), 
            ErrorMessageResourceName ="StringTooLong")]
        [MinLength(length: 2,/* ErrorMessage = "لايجب ان يتكون الاسم من حرفين او اكثر على الاقل"*/
            ErrorMessageResourceType = typeof(Resources.Login),
            ErrorMessageResourceName = "StringTooShort")]
        [Required(ErrorMessageResourceType =typeof(Resources.Global), ErrorMessageResourceName ="RequiredField")]
        public string FullName { get; set; }

        [Display(Name = "Gender", ResourceType = typeof(Resources.Login))]
        public gender_list Gender { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
