﻿using SghlaDotCom.Models.DAL.Company;
using SghlaDotCom.Models.DAL.CompanySpeciality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.Speciality
{
    public class SpecialityCompanyMRelModel
    {
        public int ID { get; set; }

        public int SpecialityModelID { get; set; }
        public int CompanyModelID { get; set; }

        public CompanyModel company { get; set; }
        public SpecialityModel speciality { get; set; }
    }
}