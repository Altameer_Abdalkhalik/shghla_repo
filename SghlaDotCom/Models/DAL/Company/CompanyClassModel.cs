﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.Company
{
    public class CompanyClassModel
    {
        public int ID { get; set; }
        public string className { get; set; }

        public List<CompanyModel> relatedCompanies { get; set; }
    }
}