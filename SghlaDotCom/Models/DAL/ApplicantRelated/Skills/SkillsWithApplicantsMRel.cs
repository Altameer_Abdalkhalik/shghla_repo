﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Skills
{
    public class SkillsWithApplicantsMRel
    {
        public int ID { get; set; }

        public int SkillsModelID { get; set; }
        public int ApplicantInfoID { get; set; }

        public ApplicantInfo user { get; set; }
        public SkillsModel skill { get; set; }
    }
}