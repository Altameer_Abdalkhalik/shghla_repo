namespace SghlaDotCom.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kjsdlgjxlfjgfi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "fullName", c => c.String(unicode: false));
            AddColumn("dbo.AspNetUsers", "gender", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "gender");
            DropColumn("dbo.AspNetUsers", "fullName");
        }
    }
}
