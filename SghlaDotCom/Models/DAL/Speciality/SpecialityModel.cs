﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.CompanySpeciality
{
    public class SpecialityModel
    {
        public int ID { get; set; }

        public string SpecialityName { get; set; }
    }
}