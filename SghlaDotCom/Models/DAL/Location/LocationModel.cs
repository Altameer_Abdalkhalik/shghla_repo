﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.Location
{
    public class LocationModel
    {
        public int ID { get; set; }

        public string country { get; set; }

        public string city { get; set; }

        public string lat { get; set; }

        public string lng { get; set; }

        public List<LocationCompanyMRelModel> companiesLocations { get; set; }

        public List<LocationOfferMRelModel> jobOffersLocations { get; set; }
    }
}