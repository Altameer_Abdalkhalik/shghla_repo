﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SghlaDotCom.Models.DAL.Offers;

namespace SghlaDotCom.Models.DAL.ExperienceLevel
{
    public class ExperienceLevelModel
    {
        public int ID { get; set; }

        public string levelName { get; set; }

        public List<OffersModel> offerList { get; set; }
    }
}