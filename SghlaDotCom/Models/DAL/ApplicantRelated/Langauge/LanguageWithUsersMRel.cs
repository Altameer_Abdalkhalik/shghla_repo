﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Langauge
{
    public class LanguageWithUsersMRel
    {
        public int ID { get; set; }

        public int LangaugeModelID { get; set; }
        public int ApplicantInfoID { get; set; }

        public ApplicantInfo user { get; set; }
        public LangaugeModel language { get; set; }
    }
}