﻿using SghlaDotCom.Models.DAL.Location;
using SghlaDotCom.Models.DAL.Speciality;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.Company
{
    public class CompanyModel
    {
        public int ID { get; set; }

        public string companyName { get; set; }

        public DateTime creationDate { get; set; }

        public string brief { get; set; }

        public string Description { get; set; }

        public int numberOfEmpolyess { get; set; }

        public int CompanyClassModelID { get; set; }
        public CompanyClassModel companyClass { get; set; }

        public string companyPhone1 { get; set; }
        public string companyPhone2 { get; set; }

        public string companyEmail1 { get; set; }
        public string companyEmail2 { get; set; }

        public List<LocationCompanyMRelModel> companyLocations { get; set; }
        public List<SpecialityCompanyMRelModel> speciality { get; set; }
        public List<EmployerInfo> employerList { get; set; }
    }
}