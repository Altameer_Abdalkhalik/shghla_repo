﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Courses
{
    // don't forget to limit this field to 100 per user
    public class CoursesModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string courseNumber { get; set; }

        public string atWhichCompanyCollege { get; set; }

        public int ApplicantInfoID { get; set; }
        public ApplicantInfo user { get; set; }
    }
}