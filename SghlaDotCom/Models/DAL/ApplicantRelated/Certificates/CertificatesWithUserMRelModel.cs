﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Certificates
{
    public class CertificatesWithUserMRelModel
    {
        public int ID { get; set; }

        public int CertificateModelID { get; set; }
        public int ApplicantInfoID { get; set; }

        public ApplicantInfo user { get; set; }
        public CertificateModel certificate { get; set; }
    }
}