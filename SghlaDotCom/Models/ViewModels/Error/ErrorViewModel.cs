﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.ViewModels.Error
{
    public class ErrorViewModel
    {
        public string Title { get; set; }

        public string subTitle { get; set; }

        public string backUrl { get; set; }
    }
}