﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.ViewModels.UsersTasks
{
    public class UsersTasksViewModel
    {
        public int ID { get; set; }

        [ScaffoldColumn(false)]
        public string madeByUser { get; set; }

        [ScaffoldColumn(false)]
        public DateTime createdAt { get; set; }

        [Display(Name = ("الى المستخدم"))]
        public string userId { get; set; }

        [Display(Name = ("الى قسم"))]
        public string Department { get; set; }

        [Display(Name = "عنوان المهمة")]
        [Required]
        public string taskTitle { get; set; }

        [Display(Name = "تفاصيل المهمة")]
        public string taskDetails { get; set; }

        [Display(Name = "حالة المهمة")]
        public string taskStatus { get; set; }
    }
}