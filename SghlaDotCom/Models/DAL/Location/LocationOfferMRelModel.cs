﻿using SghlaDotCom.Models.DAL.Offers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.Location
{
    public class LocationOfferMRelModel
    {
        public int ID { get; set; }

        public int LocationModelID { get; set; }
        public int OffersModelID { get; set; }

        public OffersModel company { get; set; }
        public LocationModel location { get; set; }
    }
}