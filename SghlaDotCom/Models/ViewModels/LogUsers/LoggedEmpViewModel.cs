﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.ViewModels.LogUsers
{
    public class LoggedEmpViewModel
    {
        public string ID { get; set; }

        public string name { get; set; }

        public string phoneNumber { get; set; }

        public string status { get; set; }

        public string branch { get; set; }

        public string role { get; set; }

        public string email { get; set; }
    }
}