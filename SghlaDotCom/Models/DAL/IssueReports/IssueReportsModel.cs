﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.IssueReports
{
    public enum reportTarget
    {
        مستخدم, طلب_توظيف
    }

    public class IssueReportsModel
    {
        public int ID { get; set; }

        public reportTarget target  { get; set; }

        public string referenceNumber { get; set; }

        public int ApplicantInfoID { get; set; }
        public ApplicantInfo reportedBy { get; set; }

        public int TypeOfReportsID { get; set; }
        public IssueReportsModel reportType { get; set; }
    }
}