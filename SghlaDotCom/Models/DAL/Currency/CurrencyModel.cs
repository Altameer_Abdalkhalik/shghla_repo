﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.Currency
{
    public class CurrencyModel
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "currencyName", ResourceType =typeof(Resources.Job))]
        public string currencyName { get; set;}
    }
}