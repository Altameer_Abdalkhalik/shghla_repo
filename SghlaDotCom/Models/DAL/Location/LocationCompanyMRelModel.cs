﻿using SghlaDotCom.Models.DAL.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.Location
{
    public class LocationCompanyMRelModel
    {
        public int ID { get; set; }

        public int LocationModelID { get; set; }
        public int CompanyModelID { get; set; }

        public CompanyModel company { get; set; }
        public LocationModel location { get; set; }
    }
}