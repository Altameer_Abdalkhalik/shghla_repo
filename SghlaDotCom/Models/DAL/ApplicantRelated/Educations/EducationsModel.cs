﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Educations
{
    // filled by admin
    public class EducationsModel
    {
        public int ID { get; set; }

        public string studyPlace { get; set; }

        // internal
        public int? rank { get; set; } 

        public DateTime startDate { get; set; }

        public DateTime? endDate { get; set; }

        public string degree { get; set; }

        public string fieldOfStudy { get; set; }

        public string grade { get; set; }

        public string description { get; set; }

        public List<EducationsWithApplicantsMRelModel> userList { get; set; }
    }
    
}