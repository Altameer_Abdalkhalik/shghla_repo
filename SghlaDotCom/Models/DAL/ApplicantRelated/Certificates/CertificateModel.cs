﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Certificates
{
    public class CertificateModel
    {
        public int ID { get; set; }

        public string title { get; set; }

        public string issuer { get; set; }

        public int? degree { get; set; }

        public DateTime takenAt { get; set; }

        public string description { get; set; }

        public List<CertificatesWithUserMRelModel> userList { get; set; }
    }
}