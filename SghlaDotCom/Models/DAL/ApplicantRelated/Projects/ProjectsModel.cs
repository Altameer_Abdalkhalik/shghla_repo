﻿using SghlaDotCom.Models.DAL.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Projects
{
    public class ProjectsModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string url { get; set; }

        public string companyName { get; set; } // if its not available yet

        public DateTime startDate { get; set; }
        public DateTime? endDate { get; set; }

        public string Description { get; set; }

        public int ApplicantInfoID { get; set; }
        public ApplicantInfo user { get; set; }
    }
}