﻿using System.Web;
using System.Web.Optimization;

namespace SghlaDotCom
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-theme.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/cssRTL").Include(
                      "~/Content/bootstrap-rtl.css",
                      "~/Content/bootstrap-theme-rtl.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/RTL").Include(
                  "~/assets/global/plugins/bootstrap/css/bootstrap.min-rtl.css",
                  "~/assets/global/plugins/uniform/css/uniform.default.css",
                  "~/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
                  "~/assets/global/plugins/morris/morris.css",
                   "~/assets/admin/pages/css/tasks.css",
                  "~/assets/global/css/components-md-rtl.css",
                  "~/assets/global/css/plugins-md-rtl.css",
                  "~/assets/admin/layout5/css/layout-rtl.css",
                  "~/assets/admin/layout5/css/custom-rtl.css"));

            bundles.Add(new StyleBundle("~/Content/LTR").Include(
                      "~/assets/global/plugins/bootstrap/css/bootstrap.min.css",
                      "~/assets/global/plugins/uniform/css/uniform.default.css",
                      "~/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
                      "~/assets/global/plugins/morris/morris.css",
                      "~/assets/admin/pages/css/tasks.css",
                      "~/assets/global/css/components-md.css",
                      "~/assets/global/css/plugins-md.css",
                      "~/assets/admin/layout5/css/layout.css",
                      "~/assets/admin/layout5/css/custom.css"));
        }
    }
}
