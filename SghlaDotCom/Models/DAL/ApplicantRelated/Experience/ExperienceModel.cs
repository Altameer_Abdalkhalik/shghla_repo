﻿using SghlaDotCom.Models.DAL.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Experience
{
    public class ExperienceModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public int CompanyModelID { get; set; }
        public CompanyModel company { get; set; }

        public DateTime startDate { get; set; }

        public DateTime? endDate { get; set; }

        public string Description { get; set; }

        public int ApplicantInfoID { get; set; }
        public ApplicantInfo applicant { get; set; }
    }
}