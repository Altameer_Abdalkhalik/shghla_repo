﻿using SghlaDotCom.Models.DAL.ApplicantRelated.Certificates;
using SghlaDotCom.Models.DAL.ApplicantRelated.Educations;
using SghlaDotCom.Models.DAL.Currency;
using SghlaDotCom.Models.DAL.ExperienceLevel;
using SghlaDotCom.Models.DAL.JobSchedule;
using SghlaDotCom.Models.DAL.Location;
using SghlaDotCom.Models.DAL.Nationality;
using SghlaDotCom.Models.DAL.PositionLevel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.Offers
{
    public enum jobReqGender
    {
        كلاهما, موظف, موظفة
    }
    /// <summary> 
        /// class for offereing something to the users of theis web app
    /// </summary> 
    /// <remarks> 
    /// </remarks> 
    public class OffersModel
    {
        public int ID { get; set; }

        // Offer Description
        public string brief { get; set; }

        public double? targetCost { get; set; } // set this
        public string salaryDescription { get; set; } // or this
         
        public int? CurrencyModel { get; set; }
        public CurrencyModel currencyModelRel { get; set; }

        public string longDescription { get; set; }

        public int rating { get; set; }

        public int numberOfRequiredEmployess { get; set; }

        public DateTime offerStartingDate { get; set; }

        public bool manageOtherPeople { get; set; }

        public int? JobScheduleModelID { get; set; }
        public JobScheduleModel scheduleType { get; set; }

        public int? ExperienceLevelModelID { get; set; }
        public ExperienceLevelModel experienceLevel { get; set; }

        // Faviourate Attributes
        public int? minExperienceYears { get; set; }
        public int? maxExperienceYears { get; set; }

        public int? PositionLevelModelID { get; set; }
        public PositionLevelModel position { get; set; }

        public List<LocationOfferMRelModel> offerlocation { get; set; }
        
        public int? EducationsModelID { get; set; }
        public EducationsModel educationDegree { get; set; }
        public bool AcceptbeyoundannouncedDegree { get; set; }

        public List<CertificateModel> certification { get; set; }

        public int? NationalityModelID { get; set; }
        public NationalityModel nation { get; set; }    

        // other stuff
        public List<FaviourateWithUserMRel> faviourateBy { get; set; }
        public List<LikesWithUserMRel> likeedBy { get; set; }
    }
}