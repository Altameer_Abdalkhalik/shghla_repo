﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.IssueReports
{
    public class TypeOfReports
    {
        public int ID { get; set; }

        public string description { get; set; }

        public List<IssueReportsModel> issueReports { get; set; }
    }
}