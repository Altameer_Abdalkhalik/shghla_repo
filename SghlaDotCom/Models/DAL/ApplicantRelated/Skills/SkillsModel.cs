﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Skills
{
    public class SkillsModel
    {
        public int ID { get; set; }

        public int order { get; set; } // used to configure its order of appereance

        public string Description { get; set; }

        public List<SkillsWithApplicantsMRel> userList { get; set; }
    }
}