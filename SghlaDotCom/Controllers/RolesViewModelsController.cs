﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SghlaDotCom.Models;
using SghlaDotCom.Models.ViewModels.Roles;
using Microsoft.AspNet.Identity.EntityFramework;
using static SghlaDotCom.Startup;

namespace SghlaDotCom.Controllers
{
    [Title(Title = "ادارة الصلاحيات")]
    public class RolesViewModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: RolesViewModels
        [Title(Title = "جدول الصلاحيات")]
        [ModifiedAuthorize]
        public ActionResult Index()
        {
            return View(db.Roles);
        }

        // GET: RolesViewModels/Details/5
        [Title(Title = "تفاصيل الصلاحيات")]
        [ModifiedAuthorize]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RolesViewModel rolesViewModel = db.Roles.Where(a => a.Id == id).Select(a => new RolesViewModel()
            {
                roleName = a.Name
            }).FirstOrDefault();

            if (rolesViewModel == null)
            {
                return HttpNotFound();
            }
            return View(rolesViewModel);
        }

        // GET: RolesViewModels/Create
        [Title(Title = "انشاء صلاحية")]
        [ModifiedAuthorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: RolesViewModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        [Log(actionDescription = "اضافة صلاحية بالنظام", typeOfAction = Models.DAL.ActivityLog.ActionType.انشاء)]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name")] IdentityRole rolesViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Roles.Add(rolesViewModel);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(rolesViewModel);
        }

        // GET: RolesViewModels/Edit/5
        [Title(Title = "تعديل صلاحية")]
        [ModifiedAuthorize]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole rolesViewModel = db.Roles.Find(id);
            if (rolesViewModel == null)
            {
                return HttpNotFound();
            }
            return View(rolesViewModel);
        }

        // POST: RolesViewModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ModifiedAuthorize]
        [Log(actionDescription = "تعديل صلاحية بالنظام", typeOfAction = Models.DAL.ActivityLog.ActionType.تعديل)]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] IdentityRole rolesViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rolesViewModel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(rolesViewModel);
        }

        // GET: RolesViewModels/Delete/5
        [Title(Title = "مسح صلاحية")]
        [ModifiedAuthorize]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IdentityRole rolesViewModel = db.Roles.Find(id);
            if (rolesViewModel == null)
            {
                return HttpNotFound();
            }
            return View(rolesViewModel);
        }

        // POST: RolesViewModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Title(Title = "تاكيد مسح صلاحية")]
        [ModifiedAuthorize]
        [Log(actionDescription = "مسح صلاحية بالنظام", typeOfAction = Models.DAL.ActivityLog.ActionType.مسح)]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            IdentityRole rolesViewModel = db.Roles.Find(id);
            db.Roles.Remove(rolesViewModel);
            await db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}