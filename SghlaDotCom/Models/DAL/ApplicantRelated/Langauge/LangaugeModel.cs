﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Langauge
{
    public class LangaugeModel
    {
        public int ID { get; set; }

        public string name { get; set; }

        public List<LanguageWithUsersMRel> userList { get; set; }
    }
}