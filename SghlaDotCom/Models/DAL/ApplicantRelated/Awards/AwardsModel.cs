﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SghlaDotCom.Models.DAL.ApplicantRelated.Awards
{
    public class AwardsModel
    {
        public int ID { get; set; }

        public string title { get; set; }

        public string atCompany { get; set; }

        public string issuedBy { get; set; }

        public DateTime date { get; set; }

        public string description { get; set; }

        public int ApplicantInfoID { get; set; }
        public ApplicantInfo user { get; set; }

    }
}